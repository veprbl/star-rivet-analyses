# BEGIN PLOT /MC_RT/d.+
LegendXPos=0.7
# END PLOT

# BEGIN PLOT /MC_RT/d01-x0.-y02
XLabel=jet $p_T$ [GeV]
LogY=0
YMin=0.
YMax=1.
# END PLOT

# BEGIN PLOT /MC_RT/d01-x01-y.+
Title=Neutral energy fraction for $|y| < 0.8$, anti-$k_T$, R=0.5, $\sqrt{s}=510$ GeV
# END PLOT

# BEGIN PLOT /MC_RT/d01-x01-y01
XLabel=r = jet neutral energy / jet total energy
YLabel=$\mathrm{d}\sigma/\mathrm{d}r$ [pb]
# END PLOT

# BEGIN PLOT /MC_RT/d01-x01-y02
YLabel=r = jet neutral energy / jet total energy
# END PLOT

# BEGIN PLOT /MC_RT/d01-x02-y.+
Title=Neutral fraction for $|y| < 0.8$, anti-$k_T$, R=0.5, $\sqrt{s}=510$ GeV
# END PLOT

# BEGIN PLOT /MC_RT/d01-x02-y01
XLabel=rt = jet neutral $p_T$ / jet total $p_T$
YLabel=$\mathrm{d}\sigma/\mathrm{d}rt$ [pb]
# END PLOT

# BEGIN PLOT /MC_RT/d01-x02-y02
YLabel=rt = jet neutral $p_T$ / jet total $p_T$
# END PLOT

# BEGIN PLOT /MC_RT/d02-.+
Title=Jet particle multiplicity for $|y| < 0.8$, anti-$k_T$, R=0.5, $\sqrt{s}=510$ GeV
YLabel=$\sigma$ [pb]
# END PLOT

# BEGIN PLOT /MC_RT/d02-x01-y01
XLabel=neutral particle multiplicity
# END PLOT

# BEGIN PLOT /MC_RT/d02-x01-y02
XLabel=charged particle multiplicity
# END PLOT

# BEGIN PLOT /MC_RT/d02-x01-y03
XLabel=neutral particle multiplicity
YLabel=charged particle multiplicity
# END PLOT

# BEGIN PLOT /MC_RT/d02-x01-y04
XLabel=tower multiplicity in bins $\Delta\eta \times \Delta\phi = 0.05 \times 0.05$
# END PLOT
