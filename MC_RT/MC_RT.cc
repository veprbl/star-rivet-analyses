// vim:et sw=2 sts=2
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/NeutralFinalState.hh"
#include "Rivet/Projections/MergedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_RT : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_RT);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Charged final state, |eta|<2.5, pT>0.2GeV
      const ChargedFinalState cfs(-2.5, 2.5, 0.2*GeV);
      // Neutral final state, |eta|<2.5, ET>0.2GeV (needed for the jets)
      const NeutralFinalState nfs(-2.5, 2.5, 0.2*GeV);
      // STAR can't see neutrons and K^0_L
      VetoedFinalState vfs(nfs);
      vfs.vetoNeutrinos();
      vfs.addVetoPairId(PID::K0L);
      vfs.addVetoPairId(PID::NEUTRON);
      // Jets are reconstructed from charged and neutral particles,
      // and the cuts are different (pT vs. ET), so we need to merge them.
      const MergedFinalState jfs(cfs, vfs);

      declare(FastJets(jfs, FastJets::ANTIKT, 0.5), "Jets");

      _h_r = bookHisto1D("d01-x01-y01", 100, 0., 1.);
      _h_pt_r = bookProfile1D("d01-x01-y02", 7, 10., 80);
      _h_rt = bookHisto1D("d01-x02-y01", 100, 0., 1.);
      _h_pt_rt = bookProfile1D("d01-x02-y02", 7, 10., 80);

      _h_n_mult = bookHisto1D("d02-x01-y01", 30, 0., 30.);
      _h_chg_mult = bookHisto1D("d02-x01-y02", 30, 0., 30.);
      _h_n_vs_chg_mult = bookHisto2D("d02-x01-y03", 30, 0., 30., 30, 0., 30.);
      _h_tower_mult = bookHisto1D("d02-x01-y04", 30, 0., 30.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 10.*GeV) && (Cuts::absrap < 0.8));
      const double weight = event.weight();

      for(const Jet& jet : jets) {
        {
          double r = jet.neutralEnergy() / jet.totalEnergy();
          _h_r->fill(r, weight);
          _h_pt_r->fill(jet.pt(), r, weight);
        }

        double pt_neutral = 0., pt_charged = 0;
        int num_neutral = 0, num_charged = 0;
        std::set<std::pair<int,int> > towers;
        for(const Particle& p : jet.particles()) {
           const PdgId pid = p.pid();
           if (PID::threeCharge(pid) == 0) {
             pt_neutral += p.pt();
             num_neutral++;
             const double PHI_BIN_SIZE = 2 * M_PI / 120;
             const double ETA_BIN_SIZE = 0.05;
             int phi_bin = p.phi() / PHI_BIN_SIZE;
             int eta_bin = p.eta() / ETA_BIN_SIZE;
             towers.insert(std::pair<int,int>(phi_bin, eta_bin));
           } else {
             pt_charged += p.pt();
             num_charged++;
           }
        }

        double rt = pt_neutral / (pt_neutral + pt_charged);
        _h_rt->fill(rt, weight);
        _h_pt_rt->fill(jet.pt(), rt, weight);
        _h_n_mult->fill(num_neutral, weight);
        _h_chg_mult->fill(num_charged, weight);
        _h_n_vs_chg_mult->fill(num_neutral, num_charged, weight);
        _h_tower_mult->fill(towers.size(), weight);
        assert(towers.size() <= num_neutral);
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_r, crossSection()/sumOfWeights());
      scale(_h_rt, crossSection()/sumOfWeights());
      scale(_h_n_mult, crossSection()/sumOfWeights());
      scale(_h_chg_mult, crossSection()/sumOfWeights());
      scale(_h_tower_mult, crossSection()/sumOfWeights());

    }

    //@}


  private:


    /// @name Histograms
    //@{
    Histo1DPtr _h_rt;
    Profile1DPtr _h_pt_rt;
    Histo1DPtr _h_r;
    Profile1DPtr _h_pt_r;
    Histo1DPtr _h_n_mult;
    Histo1DPtr _h_chg_mult;
    Histo2DPtr _h_n_vs_chg_mult;
    Histo1DPtr _h_tower_mult;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_RT);


}
