# BEGIN PLOT /STAR_2013_DIJET/d01-x01.
XLabel=$m_{jj}$ [GeV]
YLabel=$A_{LL}$
# END PLOT

# BEGIN PLOT /STAR_2013_DIJET/d01-x0.-y01
Title=Dijet $A_{LL}$ for $|y|<0.9$, anti-$k_T$, $R=0.5$
# END PLOT

# BEGIN PLOT /STAR_2013_DIJET/d01-x0.-y02
Title=Dijet $A_{LL}$ for $|y|<0.9$, EE+WW, anti-$k_T$, $R=0.5$
# END PLOT

# BEGIN PLOT /STAR_2013_DIJET/d01-x0.-y03
Title=Dijet $A_{LL}$ for $|y|<0.9$, EW+WE, anti-$k_T$, $R=0.5$
# END PLOT
