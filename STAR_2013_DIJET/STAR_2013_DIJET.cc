// vim:et sw=2 sts=2
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  class STAR_2013_DIJET : public Analysis {
  public:

    /// Constructor
    STAR_2013_DIJET()
      : Analysis("STAR_2013_DIJET")
    {    }


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      FinalState fs;
      addProjection(FastJets(fs, FastJets::ANTIKT, 0.5), "Jets");
      double _mbins[] = {17, 20, 24, 29, 34, 41, 49, 59, 70, 84, 101, 121};
      std::vector<double> mbins(_mbins, _mbins + sizeof(_mbins)/sizeof(_mbins[0]));

      _h_dijet_mass = bookHisto1D("d01-x01-y01", mbins);
      _h_ee_ww_dijet_mass = bookHisto1D("d01-x01-y02", mbins);
      _h_ew_we_dijet_mass = bookHisto1D("d01-x01-y03", mbins);

      _h_dijet_mass_fine = bookHisto1D("d01-x02-y01", (121-17)/2, 17., 121.);
      _h_ee_ww_dijet_mass_fine = bookHisto1D("d01-x02-y02", (121-17)/2, 17., 121.);
      _h_ew_we_dijet_mass_fine = bookHisto1D("d01-x02-y03", (121-17)/2, 17., 121.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = applyProjection<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt(Cuts::pT >= 7.*GeV);
      const double weight = event.weight();

      if (jets.size() < 2) return;

      const Jet& leading_jet = jets[0];
      const Jet& subleading_jet = jets[1];

      if ((leading_jet.pT() >= 10.*GeV)
       && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5)
       && (leading_jet.absrap() < 0.9)
       && (subleading_jet.absrap() < 0.9))
      {
        const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;

        _h_dijet_mass->fill(dijet_mass, weight);
        _h_dijet_mass_fine->fill(dijet_mass, weight);

        if (leading_jet.eta() * subleading_jet.eta() >= 0)
        {
          _h_ee_ww_dijet_mass->fill(dijet_mass, weight);
          _h_ee_ww_dijet_mass_fine->fill(dijet_mass, weight);
        }
        else
        {
          _h_ew_we_dijet_mass->fill(dijet_mass, weight);
          _h_ew_we_dijet_mass_fine->fill(dijet_mass, weight);
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {
      scale(_h_dijet_mass, crossSection()/sumOfWeights());
      scale(_h_ee_ww_dijet_mass, crossSection()/sumOfWeights());
      scale(_h_ew_we_dijet_mass, crossSection()/sumOfWeights());

      scale(_h_dijet_mass_fine, crossSection()/sumOfWeights());
      scale(_h_ee_ww_dijet_mass_fine, crossSection()/sumOfWeights());
      scale(_h_ew_we_dijet_mass_fine, crossSection()/sumOfWeights());
    }

    //@}


  private:

    // Data members like post-cuts event weight counters go here


    /// @name Histograms
    //@{
    Histo1DPtr _h_dijet_mass;
    Histo1DPtr _h_ee_ww_dijet_mass;
    Histo1DPtr _h_ew_we_dijet_mass;

    Histo1DPtr _h_dijet_mass_fine;
    Histo1DPtr _h_ee_ww_dijet_mass_fine;
    Histo1DPtr _h_ew_we_dijet_mass_fine;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(STAR_2013_DIJET);


}
