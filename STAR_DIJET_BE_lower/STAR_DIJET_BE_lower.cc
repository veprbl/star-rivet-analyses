// vim:et sw=2 sts=2
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  class STAR_DIJET_BE_lower : public Analysis {
  public:

    const double SQRT_S;

    /// Constructor
    STAR_DIJET_BE_lower()
      : Analysis("STAR_DIJET_BE_lower")
      , SQRT_S(200 * GeV)
    {    }


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      FinalState fs;
      addProjection(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");
      double _mbins[] = {10, 16, 19, 23, 28, 34, 41, 49, 58, 69, 82, 100, 120};
      std::vector<double> mbins(_mbins, _mbins + sizeof(_mbins)/sizeof(_mbins[0]));
      std::vector<double> xbins = logspace(1000, 1e-5, 1e0);

      _h_dijet_mass = bookHisto1D("d01-x01-y01", mbins);
      _h_ee_ww_dijet_mass = bookHisto1D("d01-x01-y02", mbins);
      _h_ew_ew_dijet_mass = bookHisto1D("d01-x01-y03", mbins);

      _h_dijet_mass_fine = bookHisto1D("d01-x02-y01", (120-10)/2, 10., 120.);
      _h_mass_eta_fine = bookHisto1D("d01-x02-y11", (120-10)/2, 10., 120.);
      _h_mass_y_fine = bookHisto1D("d01-x02-y21", (120-10)/2, 10., 120.);
      _h_ee_ww_dijet_mass_fine = bookHisto1D("d01-x02-y02", (120-10)/2, 10., 120.);
      _h_ew_ew_dijet_mass_fine = bookHisto1D("d01-x02-y03", (120-10)/2, 10., 120.);
      _h_dijet_mass_fine_othercut = bookHisto1D("d01-x03-y01", (120-10)/2, 10., 120.);

      _h_dijet_mass_fine1 = bookHisto1D("d01-x021-y01", (120-10)/1, 10., 120.);
      _h_mass_eta_fine1 = bookHisto1D("d01-x021-y11", (120-10)/1, 10., 120.);
      _h_mass_y_fine1 = bookHisto1D("d01-x021-y21", (120-10)/1, 10., 120.);
      _h_ee_ww_dijet_mass_fine1 = bookHisto1D("d01-x021-y02", (120-10)/1, 10., 120.);
      _h_ew_ew_dijet_mass_fine1 = bookHisto1D("d01-x021-y03", (120-10)/1, 10., 120.);
      _h_dijet_mass_fine1_othercut = bookHisto1D("d01-x031-y01", (120-10)/1, 10., 120.);

      _h_bjorken_x1 = bookHisto1D("d02-x01-y01", xbins);
      _h_ee_ww_bjorken_x1 = bookHisto1D("d02-x01-y02", xbins);
      _h_ew_ew_bjorken_x1 = bookHisto1D("d02-x01-y03", xbins);

      _h_bjorken_x2 = bookHisto1D("d02-x01-y01", xbins);
      _h_ee_ww_bjorken_x2 = bookHisto1D("d02-x01-y02", xbins);
      _h_ew_ew_bjorken_x2 = bookHisto1D("d02-x01-y03", xbins);

      _h_eta_fine = bookHisto1D("d03-x01-y1", 400, -2., 2.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = applyProjection<FastJets>(event, "Jets");
      const Jets& jets_other = fj.jetsByPt(Cuts::pT > 6.*GeV);
      const Jets& jets = fj.jetsByPt((Cuts::pT > 6.*GeV) && (Cuts::etaIn(-0.8, 1.8)));
      const double weight = event.weight();

      if (jets_other.size() >= 2)
      {
        const Jet& leading_jet = jets_other[0];
        const Jet& subleading_jet = jets_other[1];

        if ((leading_jet.pT() >= 8.*GeV)
         && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5))
        {
          if(((leading_jet.eta() < 0.8)
         && (subleading_jet.eta() > 0.8)) || ((leading_jet.eta() > 0.8)
         && (subleading_jet.eta() < 0.8))){
            const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;
            _h_dijet_mass_fine_othercut->fill(dijet_mass, weight);
            _h_dijet_mass_fine1_othercut->fill(dijet_mass, weight);
          }
        }
      }

      if (jets.size() < 2) return;

      const Jet& leading_jet = jets[0];
      const Jet& subleading_jet = jets[1];

      if ((leading_jet.pT() >= 8.*GeV)
       && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5))
      {
        if(((leading_jet.eta() < 0.8)
         && (subleading_jet.eta() > 0.8)) || ((leading_jet.eta() > 0.8)
         && (subleading_jet.eta() < 0.8))){

        if((leading_jet.eta() <-0.8) || (subleading_jet.eta() <-0.8)) printf ("Hello World: %f %f \n", leading_jet.eta(), subleading_jet.eta()); 
        if((leading_jet.eta() < 0.8) && (subleading_jet.eta() < 0.8)) printf ("Hello World: %f %f \n", leading_jet.eta(), subleading_jet.eta());
        if((leading_jet.eta() > 0.8) && (subleading_jet.eta() > 0.8)) printf ("Hello World: %f %f \n", leading_jet.eta(), subleading_jet.eta());
        const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;
        const double mass_eta = sqrt(
            2 * leading_jet.momentum() * subleading_jet.momentum()
            ) / GeV;
        FourMomentum p1 = leading_jet.momentum();
        FourMomentum p2 = subleading_jet.momentum();
        const double mass_y = sqrt(
            2 * p1.perp() * p2.perp() * (cosh(p1.rapidity() - p2.rapidity()) - cos(p1.phi() - p2.phi()))
            ) / GeV;
        const double x1 = (jets[0].pT() * exp(jets[0].eta()) + jets[1].pT() * exp(jets[1].eta())) / SQRT_S;
        const double x2 = (jets[0].pT() * exp(-jets[0].eta()) + jets[1].pT() * exp(-jets[1].eta())) / SQRT_S;

        _h_dijet_mass->fill(dijet_mass, weight);
        _h_dijet_mass_fine->fill(dijet_mass, weight);
        _h_mass_eta_fine->fill(mass_eta, weight);
        _h_mass_y_fine->fill(mass_y, weight);
        _h_bjorken_x1->fill(x1, weight);
        _h_bjorken_x2->fill(x2, weight);

        _h_dijet_mass_fine1->fill(dijet_mass, weight);
        _h_mass_eta_fine1->fill(mass_eta, weight);
        _h_mass_y_fine1->fill(mass_y, weight);

        _h_eta_fine->fill(jets[0].eta(), weight);
        _h_eta_fine->fill(jets[1].eta(), weight);
        if (jets[0].eta() * jets[1].eta() >= 0)
        {
          _h_ee_ww_dijet_mass->fill(dijet_mass, weight);
          _h_ee_ww_dijet_mass_fine->fill(dijet_mass, weight);
          _h_ee_ww_dijet_mass_fine1->fill(dijet_mass, weight);
          _h_ee_ww_bjorken_x1->fill(x1, weight);
          _h_ee_ww_bjorken_x2->fill(x2, weight);
        }
        else
        {
          _h_ew_ew_dijet_mass->fill(dijet_mass, weight);
          _h_ew_ew_dijet_mass_fine->fill(dijet_mass, weight);
          _h_ew_ew_dijet_mass_fine1->fill(dijet_mass, weight);
          _h_ew_ew_bjorken_x1->fill(x1, weight);
          _h_ew_ew_bjorken_x2->fill(x2, weight);
        }
      }
    }
    }

    /// Normalise histograms etc., after the run
    void finalize() {
      scale(_h_dijet_mass, crossSection()/sumOfWeights());
      scale(_h_ee_ww_dijet_mass, crossSection()/sumOfWeights());
      scale(_h_ew_ew_dijet_mass, crossSection()/sumOfWeights());

      scale(_h_dijet_mass_fine, crossSection()/sumOfWeights());
      scale(_h_mass_eta_fine, crossSection()/sumOfWeights());
      scale(_h_mass_y_fine, crossSection()/sumOfWeights());
      scale(_h_ee_ww_dijet_mass_fine, crossSection()/sumOfWeights());
      scale(_h_ew_ew_dijet_mass_fine, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_fine_othercut, crossSection()/sumOfWeights());

      scale(_h_dijet_mass_fine1, crossSection()/sumOfWeights());
      scale(_h_mass_eta_fine1, crossSection()/sumOfWeights());
      scale(_h_mass_y_fine1, crossSection()/sumOfWeights());
      scale(_h_ee_ww_dijet_mass_fine1, crossSection()/sumOfWeights());
      scale(_h_ew_ew_dijet_mass_fine1, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_fine1_othercut, crossSection()/sumOfWeights());

      scale(_h_bjorken_x1, crossSection()/sumOfWeights());
      scale(_h_ee_ww_bjorken_x1, crossSection()/sumOfWeights());
      scale(_h_ew_ew_bjorken_x1, crossSection()/sumOfWeights());

      scale(_h_bjorken_x2, crossSection()/sumOfWeights());
      scale(_h_ee_ww_bjorken_x2, crossSection()/sumOfWeights());
      scale(_h_ew_ew_bjorken_x2, crossSection()/sumOfWeights());

      scale(_h_eta_fine, crossSection()/sumOfWeights());
    }

    //@}


  private:

    // Data members like post-cuts event weight counters go here


    /// @name Histograms
    //@{
    Histo1DPtr _h_dijet_mass;
    Histo1DPtr _h_ee_ww_dijet_mass;
    Histo1DPtr _h_ew_ew_dijet_mass;

    Histo1DPtr _h_dijet_mass_fine;
    Histo1DPtr _h_mass_eta_fine;
    Histo1DPtr _h_mass_y_fine;
    Histo1DPtr _h_ee_ww_dijet_mass_fine;
    Histo1DPtr _h_ew_ew_dijet_mass_fine;
    Histo1DPtr _h_dijet_mass_fine_othercut;

    Histo1DPtr _h_dijet_mass_fine1;
    Histo1DPtr _h_mass_eta_fine1;
    Histo1DPtr _h_mass_y_fine1;
    Histo1DPtr _h_ee_ww_dijet_mass_fine1;
    Histo1DPtr _h_ew_ew_dijet_mass_fine1;
    Histo1DPtr _h_dijet_mass_fine1_othercut;

    Histo1DPtr _h_bjorken_x1;
    Histo1DPtr _h_ee_ww_bjorken_x1;
    Histo1DPtr _h_ew_ew_bjorken_x1;

    Histo1DPtr _h_bjorken_x2;
    Histo1DPtr _h_ee_ww_bjorken_x2;
    Histo1DPtr _h_ew_ew_bjorken_x2;

    Histo1DPtr _h_eta_fine;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(STAR_DIJET_BE_lower);


}
