// vim:et sw=2 sts=2
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/NeutralFinalState.hh"
#include "Rivet/Projections/MergedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_UE : public Analysis {
  private:

    double square(double x) {
      return x * x;
    }
    double Phi_mpi_pi(double phi) {
      return std::remainder(phi, 2 * M_PI);
    }
    const double REGION_AREA = 2 * M_PI / 3;
    const double CONE_R = 0.5;
    const double CONE_AREA = M_PI * CONE_R * CONE_R;

  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_UE);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Charged final state, |eta|<2.5, pT>0.2GeV
      const ChargedFinalState cfs(-2.0, 2.0, 0.2*GeV);
      // Neutral final state, |eta|<2.5, ET>0.2GeV (needed for the jets)
      const NeutralFinalState nfs(-2.0, 2.0, 0.2*GeV);
      // STAR can't see neutrons and K^0_L
      VetoedFinalState vfs(nfs);
      vfs.vetoNeutrinos();
      vfs.addVetoPairId(PID::K0L);
      vfs.addVetoPairId(PID::NEUTRON);
      // Jets are reconstructed from charged and neutral particles,
      // and the cuts are different (pT vs. ET), so we need to merge them.
      const MergedFinalState jfs(cfs, vfs);
      declare(jfs, "FS");

      declare(FastJets(jfs, FastJets::ANTIKT, 0.6), "Jets");

      _h_leading_trans_vs_cone = bookHisto2D("d01-x01-y01", 40, 0., 5., 40, 0., 5.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 10.*GeV) && (Cuts::absrap < 0.5));
      const FinalState& fs = apply<FinalState>(event, "FS");
      const double weight = event.weight();

      if (jets.size() >= 1) {
        const Jet& jet = jets[0];
        double sum_trans_pt = 0.;
        double sum_cone_pt = 0.;
        for(const Particle& p : fs.particles()) {
          if (std::fabs(p.eta()) < 1.0) {
            const double dphi = Phi_mpi_pi(p.phi() - jet.phi());
            const double deta = p.eta() - jet.eta();
            if ((std::fabs(dphi) > M_PI/3) && (std::fabs(dphi) < M_PI*2/3)) {
              sum_trans_pt += p.pt();
            }
            if (
                (square(Phi_mpi_pi(p.phi() + M_PI_2 - jet.phi())) + square(deta) < square(CONE_R))
             || (square(Phi_mpi_pi(p.phi() - M_PI_2 - jet.phi())) + square(deta) < square(CONE_R))
              ) {
              sum_cone_pt += p.pt();
            }
          }
        }
        _h_leading_trans_vs_cone->fill(
            sum_cone_pt / (2 * CONE_AREA),
            sum_trans_pt / (2 * REGION_AREA),
            weight * square(5./40.) // multiply by bin area to convert histogram to binned counter
            );
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      //scale(_h_leading_trans_vs_cone, 1000000); // multiply by bin area to convert histogram to binned counter

    }

    //@}


  private:


    /// @name Histograms
    //@{
    Histo2DPtr _h_leading_trans_vs_cone;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_UE);


}
