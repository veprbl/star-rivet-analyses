# BEGIN PLOT /MC_UE/d.+
LegendXPos=0.7
ZMin=0.9999999
# END PLOT

# BEGIN PLOT /MC_UE/d01-x01-y01
Title=$|\eta_{jet}| <= 0.5$
XLabel=Leading jet cone UE density [GeV]
YLabel=Leading jet (TransP+TransM)/2 UE density [GeV]
# END PLOT
