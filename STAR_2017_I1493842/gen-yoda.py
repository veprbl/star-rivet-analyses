#!/usr/bin/env python

"""
Generate yoda files from tsv
"""

from math import sqrt, pow

import yoda

dijet_exp = """\
19	23	1.837 0.041	0.102	0.108	-1	1.4429
23	28	6.341 0.150	0.363	0.392	-2	1.41283
28	34	2.072 0.047	0.212	0.229	-2	1.36392
34	41	6.199 0.127	0.719	0.770	-3	1.34307
41	49	2.013 0.032	0.237	0.253	-3	1.29647
49	58	6.394 0.095	0.929	0.985	-4	1.26782
58	69	1.756 0.027	0.307	0.326	-4	1.27127
69	82	4.125 0.077	0.839	0.892	-5	1.22786
82	100 6.842 0.162	1.535	1.636	-6	1.22307"""

s=yoda.Scatter2D("/REF/STAR_2017_I1493842/d01-x01-y01")

ETA_FAC = 1.6 # (-0.8, 0.8) rapidity
MB_TO_PB = 1e6

for (x_min, x_max, xsec, stat, sys_p, sys_n, mant, corr) in map(lambda s: map(float, s.split()), dijet_exp.splitlines()):
    xsec *= pow(10, mant) * ETA_FAC * MB_TO_PB / corr
    stat *= pow(10, mant) * ETA_FAC * MB_TO_PB / corr
    sys_p *= pow(10, mant) * ETA_FAC * MB_TO_PB / corr
    sys_n *= -pow(10, mant) * ETA_FAC * MB_TO_PB / corr
    binw = x_max - x_min
    s.addPoint((x_max + x_min)/2, xsec, xerrs=[binw/2, binw/2], yerrs=[sqrt(pow(stat, 2) + pow(sys_n, 2)), sqrt(pow(stat, 2) + pow(sys_p, 2))])

dijet_exp = """\
17.82 0.51 0.0066 0.0025 0.0004
21.46 0.78 0.0092 0.0023 0.0005
26.17 1.04 0.0107 0.0027 0.0005
31.69 1.28 0.0100 0.0038 0.0008
38.32 1.42 0.0099 0.0058 0.0011
48.59 1.72 0.0183 0.0084 0.0019
67.09 2.56 0.0079 0.0273 0.0034"""

s1=yoda.Scatter2D("/REF/STAR_2017_I1493842/d01-x02-y01")

for (mass, dmass, all, stat, sys) in map(lambda s: map(float, s.split()), dijet_exp.splitlines()):
    x_max = mass + dmass
    x_min = mass - dmass
    binw = x_max - x_min
    s1.addPoint(mass, all, xerrs=[binw/2, binw/2], yerrs=[sqrt(pow(stat, 2) + pow(sys, 2)), sqrt(pow(stat, 2) + pow(sys, 2))])

dijet_exp = """\
17.70 0.56 0.0071 0.0034 0.0004
21.33 1.07 0.0088 0.0032 0.0005
26.02 1.33 0.0158 0.0039 0.0006
31.66 1.39 0.0030 0.0057 0.0010
38.25 1.79 0.0131 0.0090 0.0015
48.29 2.17 0.0338 0.0133 0.0024
66.65 2.56 0.0755 0.0460 0.0041"""

s2=yoda.Scatter2D("/REF/STAR_2017_I1493842/d01-x02-y02")

for (mass, dmass, all, stat, sys) in map(lambda s: map(float, s.split()), dijet_exp.splitlines()):
    x_max = mass + dmass
    x_min = mass - dmass
    binw = x_max - x_min
    s2.addPoint(mass, all, xerrs=[binw/2, binw/2], yerrs=[sqrt(pow(stat, 2) + pow(sys, 2)), sqrt(pow(stat, 2) + pow(sys, 2))])


dijet_exp = """\
17.99 0.54 0.0060 0.0039 0.0006
21.58 0.96 0.0096 0.0032 0.0006
26.31 1.32 0.0066 0.0037 0.0007
31.71 1.72 0.0159 0.0050 0.0009
38.38 1.70 0.0077 0.0077 0.0013
48.79 2.06 0.0085 0.0109 0.0022
67.32 3.35 -0.0279 0.0340 0.0036"""

s3=yoda.Scatter2D("/REF/STAR_2017_I1493842/d01-x02-y03")

for (mass, dmass, all, stat, sys) in map(lambda s: map(float, s.split()), dijet_exp.splitlines()):
    x_max = mass + dmass
    x_min = mass - dmass
    binw = x_max - x_min
    s3.addPoint(mass, all, xerrs=[binw/2, binw/2], yerrs=[sqrt(pow(stat, 2) + pow(sys, 2)), sqrt(pow(stat, 2) + pow(sys, 2))])


yoda.writeYODA([s, s1, s2, s3], "STAR_2017_I1493842.yoda")
