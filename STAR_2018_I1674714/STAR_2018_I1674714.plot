# BEGIN PLOT /STAR_2018_I1674714/d01-x01-y15
Title=Dijet spectrum for $-0.8 < \eta_1 < 0$, $0.8 < \eta_2 < 1.8$, anti-$k_T$, $R=0.6$
# END PLOT
# BEGIN PLOT /STAR_2018_I1674714/d01-x01-y16
Title=Dijet spectrum for $0 < \eta_1 < 0.8$, $0.8 < \eta_2 < 1.8$, anti-$k_T$, $R=0.6$
# END PLOT
# BEGIN PLOT /STAR_2018_I1674714/d01-x01-y17
Title=Dijet spectrum for $0.8 < \eta_1 < 1.8$, $0.8 < \eta_2 < 1.8$, anti-$k_T$, $R=0.6$
# END PLOT
