// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class STAR_2018_I1674714 : public Analysis {
  private:

    bool in_eastbarrel(const Jet& jet) {
      return (-0.8 <= jet.eta()) && (jet.eta() <= 0.0);
    }

    bool in_westbarrel(const Jet& jet) {
      return (0.0 <= jet.eta()) && (jet.eta() <= 0.8);
    }

    bool in_endcap(const Jet& jet) {
      return (0.8 <= jet.eta()) && (jet.eta() < 1.8);
    }
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(STAR_2018_I1674714);


    /// @name Analysis methods
    ///@{

    /// Book histograms and initialise projections before the run
    void init() {

      const FinalState fs;

      FastJets jetfs(fs, FastJets::ANTIKT, 0.6);
      declare(jetfs, "jets");

      double _mbins[] = {16, 19, 23, 28, 34, 41, 58, 82};
      std::vector<double> mbins(_mbins, _mbins + sizeof(_mbins)/sizeof(_mbins[0]));

#if RIVET_VERSION_CODE < 30000
      _h["eastb_endcap"] = bookHisto1D("d01-x01-y15", mbins);
      _h["westb_endcap"] = bookHisto1D("d01-x01-y16", mbins);
      _h["endcap_endcap"] = bookHisto1D("d01-x01-y17", mbins);
#else
      book(_h["eastb_endcap"], "d01-x01-y15", mbins);
      book(_h["westb_endcap"], "d01-x01-y16", mbins);
      book(_h["endcap_endcap"], "d01-x01-y17", mbins);
#endif

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      Jets jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 6*GeV);
#if RIVET_VERSION_CODE < 30000
      const double weight = event.weight();
#endif

      if (jets.size() >= 2) {
        const Jet& leading_jet = jets[0];
        const Jet& subleading_jet = jets[1];
        const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;

        if ((leading_jet.pt() > 8*GeV)
         && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5)) {
          if ((in_eastbarrel(leading_jet) && in_endcap(subleading_jet))
              || (in_eastbarrel(subleading_jet) && in_endcap(leading_jet))) {
            _h["eastb_endcap"]->fill(dijet_mass
#if RIVET_VERSION_CODE < 30000
              , weight
#endif
            );
          }
          if ((in_westbarrel(leading_jet) && in_endcap(subleading_jet))
              || (in_westbarrel(subleading_jet) && in_endcap(leading_jet))) {
            _h["westb_endcap"]->fill(dijet_mass
#if RIVET_VERSION_CODE < 30000
              , weight
#endif
            );
          }
          if (in_endcap(leading_jet) && in_endcap(subleading_jet)) {
            _h["endcap_endcap"]->fill(dijet_mass
#if RIVET_VERSION_CODE < 30000
              , weight
#endif
            );
          }
        }
      }

    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h["eastb_endcap"], crossSection()/sumOfWeights());
      scale(_h["westb_endcap"], crossSection()/sumOfWeights());
      scale(_h["endcap_endcap"], crossSection()/sumOfWeights());

    }

    ///@}


    /// @name Histograms
    ///@{
    map<string, Histo1DPtr> _h;
    ///@}


  };


  DECLARE_RIVET_PLUGIN(STAR_2018_I1674714);

}
