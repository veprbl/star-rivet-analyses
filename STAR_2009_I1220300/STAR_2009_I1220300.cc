// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

#include "fastjet/D0RunIIConePlugin.hh"

namespace Rivet {


  /// @brief STAR inclusive jet and dijet cross-section in pp at 200 GeV
  class STAR_2009_I1220300 : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(STAR_2009_I1220300);

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      const FinalState fs;
      const double R = 0.7, min_jet_Et = 8.0, split_ratio = 0.5;
      fastjet::JetDefinition::Plugin *plugin = new fastjet::D0RunIIConePlugin(R, min_jet_Et, split_ratio);
      addProjection(FastJets(fs, plugin), "MidpointJets");

      _h_incl_jet_pT = bookHisto1D(1, 1, 1);
      _h_dijet_mjj = bookHisto1D(2, 1, 1);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const double weight = event.weight();
      const FastJets& fj = applyProjection<FastJets>(event, "MidpointJets");
      const Jets& inclusive_jets = fj.jets(Cuts::ptIn(12.69 * GeV, 66.49 * GeV) && Cuts::absrap < 0.8);

      foreach(const Jet& j, inclusive_jets) {
        _h_incl_jet_pT->fill(j.pT(), weight);
      }

      const Jets& dijets = fj.jetsByPt((Cuts::pT > 7.*GeV) && (Cuts::absrap < 0.8));

      if (dijets.size() >= 2) {
        const Jet& leading_jet = dijets[0];
        const Jet& subleading_jet = dijets[1];

        if ((leading_jet.pT() >= 10.*GeV)
         && (abs(leading_jet.eta() - subleading_jet.eta()) < 1.0)
         && (abs(leading_jet.phi() - subleading_jet.phi()) > 2.0))
        {
          const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;
          _h_dijet_mjj->fill(dijet_mass, weight);
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {
      scale(_h_incl_jet_pT, crossSection()/picobarn/sumOfWeights()/(2*0.8*2*M_PI));
      scale(_h_dijet_mjj, crossSection()/picobarn/sumOfWeights()/(2*0.8*2*0.8));
    }

    //@}


  private:


    /// @name Histograms
    //@{
    Histo1DPtr _h_incl_jet_pT;
    Histo1DPtr _h_dijet_mjj;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(STAR_2009_I1220300);

}
