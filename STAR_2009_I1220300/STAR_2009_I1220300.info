Name: STAR_2009_I1220300
Year: 2009
Summary: Inclusive jet and dijet cross-section in pp at 200 GeV
Experiment: STAR
Collider: RHIC pp 200 GeV
InspireID: 1220300
Status: UNVALIDATED
Authors:
 - Dmitry Kalinkin <dmitry.kalinkin@gmail.com>
References:
 - Tai Sakuma, "Inclusive Jet and Dijet Production in Polarized Proton-proton Collisions at 200 GeV at RHIC", ScD thesis (2010), Massachusetts Institute of Technology
RunInfo:
  <Insert event types (not gen-specific), energy, any kinematic
  efficiency cut(s) that may be needed; essentially any details needed to set
  up a generator to reproduce the data.>
NumEvents: 1000000
Beams: [p+, p+]
Energies: [200]
PtCuts: [13, 66]
NeedCrossSection: True
Description:
  Compared to STAR_2006_S6870392, this analysis provides statistics increased
  from 0.3 to 5.4 pb${}^{-1}$ as well as inclusion of east side of the BEMC.
  Jet cone radius was ajusted from 0.4 to 0.7 and $p_T$ range changed from 5-50
  GeV to 13-66 GeV.  Cross section uncertainties are given by statistical and
  systematic uncertainties summed in quadratures. 7.6% systematic uncertainty
  of the integrated luminosity is not included.
BibKey: Sakuma:2010qea
BibTeX: '@article{Sakuma:2010qea,
      author         = "Sakuma, Tai",
      title          = "{Inclusive Jet and Dijet Production in Polarized
                        Proton-proton Collisions at $\sqrt{s}$ = 200 GeV at RHIC}",
      SLACcitation   = "%%CITATION = INSPIRE-1220300;%%",
}'
ToDo:
 - Implement the analysis, test it, remove this ToDo, and mark as VALIDATED :-)

