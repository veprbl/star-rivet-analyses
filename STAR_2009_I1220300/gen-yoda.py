#!/usr/bin/env python

"""
Generate yoda files from tsv
"""

from math import sqrt, pow

import yoda

incljet_exp = """\
12.69	14.08	1.19	0.11	+0.41	-0.30	5
14.08	15.61	5.18	0.26	+1.89	-1.48	4
15.61	17.31	2.12	0.08	+0.77	-0.57	4
17.31	19.20	9.82	0.38	+3.54	-2.75	3
19.20	21.30	4.31	0.16	+1.53	-1.17	3
21.30	23.62	1.81	0.06	+0.62	-0.48	3
23.62	26.19	7.92	0.27	+2.68	-2.22	2
26.19	29.05	3.34	0.12	+1.07	-0.88	2
29.05	32.22	1.47	0.06	+0.46	-0.40	2
32.22	35.73	5.33	0.26	+1.72	-1.48	1
35.73	39.63	1.91	0.09	+0.69	-0.58	1
39.63	43.95	6.29	0.38	+2.34	-1.75	0
43.95	48.74	1.91	0.11	+0.79	-0.79	0
48.74	54.06	4.44	0.42	+1.67	-1.35	-1
54.06	59.96	7.97	1.29	+5.78	-3.11	-2
59.96	66.49	1.15	0.30	+0.46	-0.68	-2"""

s1=yoda.Scatter2D("/REF/STAR_2009_I1220300/d01-x01-y01")

for (x_min, x_max, xsec, stat, sys_p, sys_n, mant) in map(lambda s: map(float, s.split()), incljet_exp.splitlines()):
    xsec *= pow(10, mant)
    stat *= pow(10, mant)
    sys_p *= pow(10, mant)
    sys_n *= -pow(10, mant)
    # 7.6% luminosity uncertainty not included
    binw = x_max - x_min
    s1.addPoint((x_max + x_min)/2, xsec, xerrs=[binw/2, binw/2], yerrs=[sqrt(pow(stat, 2) + pow(sys_n, 2)), sqrt(pow(stat, 2) + pow(sys_p, 2))])

dijet_exp = """\
24.00	28.60	7.91	0.88	+2.56	-1.86	4
28.60	32.50	3.09	0.24	+0.87	-0.74	4
32.50	36.00	1.29	0.12	+0.39	-0.31	4
36.00	40.90	4.77	0.26	+1.46	-1.19	3
40.90	46.70	2.07	0.11	+0.56	-0.48	3
46.70	53.60	7.35	0.38	+2.11	-1.89	2
53.60	62.00	2.56	0.12	+0.73	-0.63	2
62.00	72.20	7.84	0.36	+2.75	-2.13	1
72.20	84.50	1.92	0.12	+0.51	-0.69	1
84.50	99.60	2.97	0.35	+2.08	-0.78	0
99.60	118.00	4.59	1.10	+2.35	-2.71	-1"""

s2=yoda.Scatter2D("/REF/STAR_2009_I1220300/d02-x01-y01")

for (x_min, x_max, xsec, stat, sys_p, sys_n, mant) in map(lambda s: map(float, s.split()), dijet_exp.splitlines()):
    xsec *= pow(10, mant)
    stat *= pow(10, mant)
    sys_p *= pow(10, mant)
    sys_n *= -pow(10, mant)
    # 7.6% luminosity uncertainty not included
    binw = x_max - x_min
    s2.addPoint((x_max + x_min)/2, xsec, xerrs=[binw/2, binw/2], yerrs=[sqrt(pow(stat, 2) + pow(sys_n, 2)), sqrt(pow(stat, 2) + pow(sys_p, 2))])

yoda.writeYODA([s1, s2], "STAR_2009_I1220300.yoda")
