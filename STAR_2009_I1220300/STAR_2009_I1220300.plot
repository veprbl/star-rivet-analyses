# BEGIN PLOT /STAR_2009_I1220300/d01-x01-y01
Title=Inclusive jet cross-section $|y| < 0.8$, mid-cone, $R=0.7$
XLabel=jet $p_\perp$/GeV
YLabel=$1/(2\pi) \, d^2\sigma/(d\eta dp_\perp)$ [pb/GeV]
FullRange=1
# END PLOT

# BEGIN PLOT /STAR_2009_I1220300/d02-x01-y01
Title=Dijet mass spectrum for $|y| < 0.8$, mid-cone, $R=0.7$
XLabel=jet $M_{jj}$/GeV
YLabel=$d^3\sigma/(dM_{jj} d\eta_3 dp_\eta_4)$ [pb/GeV]
FullRange=1
# END PLOT

