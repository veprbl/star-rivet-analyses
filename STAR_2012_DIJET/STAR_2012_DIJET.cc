//2012 500GeV Dijets - 4 Topologies

// vim:et sw=2 sts=2
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  class STAR_2012_DIJET : public Analysis {
  public:

    /// Constructor
    STAR_2012_DIJET()
      : Analysis("STAR_2012_DIJET")
    {    }


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      FinalState fs;
      addProjection(FastJets(fs, FastJets::ANTIKT, 0.5), "Jets");

      //  double _mbins[] = {12, 14, 17, 20, 24, 29, 34, 41, 49, 59, 70, 84, 101, 121, 145};
      double _mbins[] = {5, 5.3, 5.6, 5.9, 6.2, 6.5, 6.8, 7.1, 7.4, 7.7, 8, 8.3, 8.6, 8.9, 9.2, 9.5, 9.8, 10.1, 10.4, 10.7, 11, 11.3, 11.6, 11.9, 12.2, 12.5, 12.8, 13.1, 13.4, 13.7, 14, 14.3, 14.6, 14.9, 15.2, 15.5, 15.8, 16.1, 16.4, 16.7, 17, 17.3, 17.6, 17.9, 18.2, 18.5, 18.8, 19.1, 19.4, 19.7, 20, 20.4, 20.8, 21.2, 21.6, 22, 22.4, 22.8, 23.2, 23.6, 24, 24.5, 25, 25.5, 26, 26.5, 27, 27.5, 28, 28.5, 29, 29.5, 30, 30.5, 31, 31.5, 32, 32.5, 33, 33.5, 34, 34.7, 35.4, 36.1, 36.8, 37.5, 38.2, 38.9, 39.6, 40.3, 41, 41.8, 42.6, 43.4, 44.2, 45, 45.8, 46.6, 47.4, 48.2, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61.2, 63.4, 65.6, 67.8, 70, 72.8, 75.6, 78.4, 81.2, 84, 87.4, 90.8, 94.2, 97.6, 101, 105, 109, 113, 117, 121, 125.8, 130.6, 135.4, 140.2, 145, 150};
      std::vector<double> mbins(_mbins, _mbins + sizeof(_mbins)/sizeof(_mbins[0]));

      _h_dijet_mass_1 = bookHisto1D("d01-x01-y01", mbins);
      _h_dijet_mass_2 = bookHisto1D("d01-x02-y01", (150-5)/2, 5., 150.);
      _h_dijet_mass_3 = bookHisto1D("d01-x03-y01", (150-5)/4, 5., 150.);

      _h_dijet_mass1_1 = bookHisto1D("d01-x01-y01-bin1", mbins);
      _h_dijet_mass1_2 = bookHisto1D("d01-x02-y01-bin1", (150-5)/2, 5., 150.);
      _h_dijet_mass1_3 = bookHisto1D("d01-x03-y01-bin1", (150-5)/4, 5., 150.);

      _h_dijet_mass2_1 = bookHisto1D("d01-x01-y01-bin2", mbins);
      _h_dijet_mass2_2 = bookHisto1D("d01-x02-y01-bin2", (150-5)/2, 5., 150.);
      _h_dijet_mass2_3 = bookHisto1D("d01-x03-y01-bin2", (150-5)/4, 5., 150.);

      _h_dijet_mass3_1 = bookHisto1D("d01-x01-y01-bin3", mbins);
      _h_dijet_mass3_2 = bookHisto1D("d01-x02-y01-bin3", (150-5)/2, 5., 150.);
      _h_dijet_mass3_3 = bookHisto1D("d01-x03-y01-bin3", (150-5)/4, 5., 150.);

      _h_dijet_mass4_1 = bookHisto1D("d01-x01-y01-bin4", mbins);
      _h_dijet_mass4_2 = bookHisto1D("d01-x02-y01-bin4", (150-5)/2, 5., 150.);
      _h_dijet_mass4_3 = bookHisto1D("d01-x03-y01-bin4", (150-5)/4, 5., 150.);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = applyProjection<FastJets>(event, "Jets");
      //  const Jets& jets = fj.jetsByPt((Cuts::pT > 6.*GeV) && (Cuts::absrap < 0.8));
      const Jets& jets = fj.jetsByPt((Cuts::pT > 6.*GeV));
      const double weight = event.weight();

      if (jets.size() < 2) return;

      const Jet& leading_jet = jets[0];
      const Jet& subleading_jet = jets[1];
      
      int Bin1 = 0;
      int Bin2 = 0;
      int Bin3 = 0;
      int Bin4 = 0;
      
      if((leading_jet.rap() >= 0.3 && leading_jet.rap() < 0.9) && (subleading_jet.rap() >= 0.3 && subleading_jet.rap() < 0.9)) Bin1 = 1;
      if((leading_jet.rap() >= -0.9 && leading_jet.rap() < -0.3) && (subleading_jet.rap() >= -0.9 && subleading_jet.rap() < -0.3)) Bin1 = 1;
      if((leading_jet.rap() >= 0.3 && leading_jet.rap() < 0.9) && (subleading_jet.rap() >= -0.3 && subleading_jet.rap() < 0.3)) Bin2 = 1;
      if((leading_jet.rap() >= -0.3 && leading_jet.rap() < 0.3) && (subleading_jet.rap() >= 0.3 && subleading_jet.rap() < 0.9)) Bin2 = 1;
      if((leading_jet.rap() >= -0.9 && leading_jet.rap() < -0.3) && (subleading_jet.rap() >= -0.3 && subleading_jet.rap() < 0.3)) Bin2 = 1;
      if((leading_jet.rap() >= -0.3 && leading_jet.rap() < 0.3) && (subleading_jet.rap() >= -0.9 && subleading_jet.rap() < -0.3)) Bin2 = 1;
      if((leading_jet.rap() >= 0.3 && leading_jet.rap() < 0.9) && (subleading_jet.rap() >= -0.9 && subleading_jet.rap() < -0.3)) Bin3 = 1;
      if((leading_jet.rap() >= -0.9 && leading_jet.rap() < -0.3) && (subleading_jet.rap() >= 0.3 && subleading_jet.rap() < 0.9)) Bin3 = 1;
      if((leading_jet.rap() >= -0.3 && leading_jet.rap() < 0.3) && (subleading_jet.rap() >= -0.3 && subleading_jet.rap() < 0.3)) Bin4 = 1;

      //Full Topology      
      if ((leading_jet.pT() >= 8.*GeV) && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5)){	
	const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;
	_h_dijet_mass_1->fill(dijet_mass, weight);
	_h_dijet_mass_2->fill(dijet_mass, weight);
	_h_dijet_mass_3->fill(dijet_mass, weight);
      }

      //Bin1
      if ((leading_jet.pT() >= 8.*GeV) && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5) && (Bin1 == 1)){	
	const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;
	_h_dijet_mass1_1->fill(dijet_mass, weight);
	_h_dijet_mass1_2->fill(dijet_mass, weight);
	_h_dijet_mass1_3->fill(dijet_mass, weight);
      }
      
      //Bin2
      if ((leading_jet.pT() >= 8.*GeV) && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5) && (Bin2 == 1)){	
	const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;
	_h_dijet_mass2_1->fill(dijet_mass, weight);
	_h_dijet_mass2_2->fill(dijet_mass, weight);
	_h_dijet_mass2_3->fill(dijet_mass, weight);
      }
      
      //Bin3
      if ((leading_jet.pT() >= 8.*GeV) && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5) && (Bin3 == 1)){	
	const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;
	_h_dijet_mass3_1->fill(dijet_mass, weight);
	_h_dijet_mass3_2->fill(dijet_mass, weight);
	_h_dijet_mass3_3->fill(dijet_mass, weight);
      }

      //Bin4
      if ((leading_jet.pT() >= 8.*GeV) && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5) && (Bin4 == 1)){	
	const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;
	_h_dijet_mass4_1->fill(dijet_mass, weight);
	_h_dijet_mass4_2->fill(dijet_mass, weight);
	_h_dijet_mass4_3->fill(dijet_mass, weight);
      }
      
    }
    
    
    /// Normalise histograms etc., after the run
    void finalize() {
      scale(_h_dijet_mass_1, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_2, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_3, crossSection()/sumOfWeights());

      scale(_h_dijet_mass1_1, crossSection()/sumOfWeights());
      scale(_h_dijet_mass1_2, crossSection()/sumOfWeights());
      scale(_h_dijet_mass1_3, crossSection()/sumOfWeights());

      scale(_h_dijet_mass2_1, crossSection()/sumOfWeights());
      scale(_h_dijet_mass2_2, crossSection()/sumOfWeights());
      scale(_h_dijet_mass2_3, crossSection()/sumOfWeights());

      scale(_h_dijet_mass3_1, crossSection()/sumOfWeights());
      scale(_h_dijet_mass3_2, crossSection()/sumOfWeights());
      scale(_h_dijet_mass3_3, crossSection()/sumOfWeights());

      scale(_h_dijet_mass4_1, crossSection()/sumOfWeights());
      scale(_h_dijet_mass4_2, crossSection()/sumOfWeights());
      scale(_h_dijet_mass4_3, crossSection()/sumOfWeights());
    }

    //@}


  private:

    // Data members like post-cuts event weight counters go here


    /// @name Histograms
    //@{
    Histo1DPtr _h_dijet_mass_1;
    Histo1DPtr _h_dijet_mass_2;
    Histo1DPtr _h_dijet_mass_3;

    Histo1DPtr _h_dijet_mass1_1;
    Histo1DPtr _h_dijet_mass1_2;
    Histo1DPtr _h_dijet_mass1_3;

    Histo1DPtr _h_dijet_mass2_1;
    Histo1DPtr _h_dijet_mass2_2;
    Histo1DPtr _h_dijet_mass2_3;

    Histo1DPtr _h_dijet_mass3_1;
    Histo1DPtr _h_dijet_mass3_2;
    Histo1DPtr _h_dijet_mass3_3;

    Histo1DPtr _h_dijet_mass4_1;
    Histo1DPtr _h_dijet_mass4_2;
    Histo1DPtr _h_dijet_mass4_3;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(STAR_2012_DIJET);


}
