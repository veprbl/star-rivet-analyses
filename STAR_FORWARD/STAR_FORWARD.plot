# BEGIN PLOT /STAR_FORWARD/d.
# END PLOT

# BEGIN PLOT /STAR_FORWARD.*/d0.-x01.
XLabel=$m_{jj} / \sqrt{s}$
# END PLOT
# BEGIN PLOT /STAR_FORWARD/d0.-x01.
YLabel=$(d\sigma/dm_{jj}) \sqrt{s}$, [pb]
LegendXPos=0.3
LegendYPos=0.25
# END PLOT
# BEGIN PLOT /STAR_FORWARD_ALL/d0.-x01.
YLabel=$A_{\text{LL}}$
LegendXPos=0.05
LegendYPos=0.3
# END PLOT

# BEGIN PLOT /STAR_FORWARD/d0.-x01-y01
Title=\hspace{-35pt}Di-jet x-sec, $\sqrt{s}=510$~GeV, $-0.8 < \eta_1 < 0$, $2.8 < \eta_2 < 3.7$, a-$k_T$, $R=0.6$
# END PLOT

# BEGIN PLOT /STAR_FORWARD_ALL/d0.-x01-y01
Title=\hspace{-35pt}Di-jet $A_{\text{LL}}$, $\sqrt{s}=510$~GeV, $-0.8 < \eta_1 < 0$, $2.8 < \eta_2 < 3.7$, a-$k_T$, $R=0.6$
YMax=0.002
YMin=-0.0015
# END PLOT

# BEGIN PLOT /STAR_FORWARD/d0.-x01-y02
Title=\hspace{-35pt}Di-jet x-sec, $\sqrt{s}=510$~GeV, $0 < \eta_1 < 0.8$, $2.8 < \eta_2 < 3.7$, a-$k_T$, $R=0.6$
# END PLOT

# BEGIN PLOT /STAR_FORWARD_ALL/d0.-x01-y02
Title=\hspace{-35pt}Di-jet $A_{\text{LL}}$, $\sqrt{s}=510$~GeV, $0 < \eta_1 < 0.8$, $2.8 < \eta_2 < 3.7$, a-$k_T$, $R=0.6$
YMax=0.0015
YMin=-0.00125
# END PLOT

# BEGIN PLOT /STAR_FORWARD/d0.-x01-y03
Title=\hspace{-35pt}Di-jet x-sec, $\sqrt{s}=510$~GeV, $1.2 < \eta_1 < 1.8$, $2.8 < \eta_2 < 3.7$, a-$k_T$, $R=0.6$
# END PLOT

# BEGIN PLOT /STAR_FORWARD_ALL/d0.-x01-y03
Title=\hspace{-35pt}Di-jet $A_{\text{LL}}$, $\sqrt{s}=510$~GeV, $1.2 < \eta_1 < 1.8$, $2.8 < \eta_2 < 3.7$, a-$k_T$, $R=0.6$
YMax=0.002
YMin=-0.00125
# END PLOT

# BEGIN PLOT /STAR_FORWARD/d0.-x01-y04
Title=\hspace{-35pt}Di-jet x-sec, $\sqrt{s}=510$~GeV, $2.8 < \eta_1 < 3.7$, $2.8 < \eta_2 < 3.7$, a-$k_T$, $R=0.6$
# END PLOT

# BEGIN PLOT /STAR_FORWARD_ALL/d0.-x01-y04
Title=\hspace{-35pt}Di-jet $A_{\text{LL}}$, $\sqrt{s}=510$~GeV, $2.8 < \eta_1 < 3.7$, $2.8 < \eta_2 < 3.7$, a-$k_T$, $R=0.6$
YMax=0.002
YMin=-0.0015
# END PLOT
