// vim:et sw=2 sts=2
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  class STAR_FORWARD : public Analysis {
  public:

    constexpr static const double SQRT_S = 510.;

    /// Constructor
    STAR_FORWARD()
      : Analysis("STAR_FORWARD")
    {    }


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      FinalState fs;
      declare(FastJets(fs, FastJets::ANTIKT, 0.6), "Jets");

      _h_dijet_mass_nbarrel_forward = bookHisto1D("d01-x01-y01", std::vector<double>({ 0.06, 0.08, 0.12 }));
      _h_dijet_mass_pbarrel_forward = bookHisto1D("d01-x01-y02", std::vector<double>({ 0.04, 0.057, 0.077, 0.12 }));
      _h_dijet_mass_endcap_forward = bookHisto1D("d01-x01-y03", std::vector<double>({ 0.02, 0.04, 0.06, 0.08 }));
      _h_dijet_mass_forward_forward = bookHisto1D("d01-x01-y04", std::vector<double>({ 0.02, 0.04 }));

      _h_dijet_mass_nbarrel_forward_fine = bookHisto1D("d02-x01-y01", 100, 40., 100.);
      _h_dijet_mass_pbarrel_forward_fine = bookHisto1D("d02-x01-y02", 100, 30., 100.);
      _h_dijet_mass_endcap_forward_fine = bookHisto1D("d02-x01-y03", 100, 23, 70);
      _h_dijet_mass_forward_forward_fine = bookHisto1D("d02-x01-y04", 100, 13, 40);

      std::vector<double> xbins = logspace(100, 1e-5, 1e0);

      _h_x_nbarrel_forward_x1 = bookHisto1D("d03-x01-y01", xbins);
      _h_x_nbarrel_forward_x2 = bookHisto1D("d03-x02-y01", xbins);
      _h_x_pbarrel_forward_x1 = bookHisto1D("d03-x01-y02", xbins);
      _h_x_pbarrel_forward_x2 = bookHisto1D("d03-x02-y02", xbins);
      _h_x_endcap_forward_x1 = bookHisto1D("d03-x01-y03", xbins);
      _h_x_endcap_forward_x2 = bookHisto1D("d03-x02-y03", xbins);
      _h_x_forward_forward_x1 = bookHisto1D("d03-x01-y04", xbins);
      _h_x_forward_forward_x2 = bookHisto1D("d03-x02-y04", xbins);
    }

    static bool nbarrel(double eta) {
      return (eta > -0.8) && (eta < 0);
    }

    static bool pbarrel(double eta) {
      return (eta > 0) && (eta < 0.8);
    }

    static bool endcap(double eta) {
      return (eta > 1.2) && (eta < 1.8);
    }

    static bool forward(double eta) {
      return (eta > 2.8) && (eta < 3.7);
    }

    bool match_regions(double eta1, double eta2, std::function<bool(double)> region1, std::function<bool(double)> region2) {
      return (region1(eta1) && region2(eta2)) || (region1(eta2) && region2(eta1));
    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {
      const FastJets& fj = apply<FastJets>(event, "Jets");
      const Jets& jets = fj.jetsByPt((Cuts::pT > 6.*GeV) && (Cuts::rap > -0.8) && (Cuts::rap < 3.7));
      const double weight = event.weight();
      const HepMC::PdfInfo *pdf_info = event.genEvent()->pdf_info();

      if (jets.size() < 2) return;

      const Jet& leading_jet = jets[0];
      const Jet& subleading_jet = jets[1];

      if ((leading_jet.pT() >= 8.*GeV)
       && (cos(leading_jet.phi() - subleading_jet.phi()) <= -0.5))
      {
        const double dijet_mass = (leading_jet.momentum() + subleading_jet.momentum()).mass() / GeV;

        if (match_regions(jets[0].eta(), jets[1].eta(), nbarrel, forward)) {
          _h_dijet_mass_nbarrel_forward->fill(dijet_mass / SQRT_S, weight);
          _h_dijet_mass_nbarrel_forward_fine->fill(dijet_mass, weight);
        }
        if (match_regions(jets[0].eta(), jets[1].eta(), pbarrel, forward)) {
          _h_dijet_mass_pbarrel_forward->fill(dijet_mass / SQRT_S, weight);
          _h_dijet_mass_pbarrel_forward_fine->fill(dijet_mass, weight);
        }
        if (match_regions(jets[0].eta(), jets[1].eta(), endcap, forward)) {
          _h_dijet_mass_endcap_forward->fill(dijet_mass / SQRT_S, weight);
          _h_dijet_mass_endcap_forward_fine->fill(dijet_mass, weight);
        }
        if (match_regions(jets[0].eta(), jets[1].eta(), forward, forward)) {
          _h_dijet_mass_forward_forward->fill(dijet_mass / SQRT_S, weight);
          _h_dijet_mass_forward_forward_fine->fill(dijet_mass, weight);
        }

        if (pdf_info) {
          const double x1 = pdf_info->x1();
          const double x2 = pdf_info->x2();

          if (
            match_regions(jets[0].eta(), jets[1].eta(), nbarrel, forward)
            && (_h_dijet_mass_nbarrel_forward_fine->xMin() <= dijet_mass)
            && (_h_dijet_mass_nbarrel_forward_fine->xMax() > dijet_mass)
            ) {
            _h_x_nbarrel_forward_x1->fill(x1, weight * x1);
            _h_x_nbarrel_forward_x2->fill(x2, weight * x2);
          }
          if (
            match_regions(jets[0].eta(), jets[1].eta(), pbarrel, forward)
            && (_h_dijet_mass_pbarrel_forward_fine->xMin() <= dijet_mass)
            && (_h_dijet_mass_pbarrel_forward_fine->xMax() > dijet_mass)
            ) {
            _h_x_pbarrel_forward_x1->fill(x1, weight * x1);
            _h_x_pbarrel_forward_x2->fill(x2, weight * x2);
          }
          if (
            match_regions(jets[0].eta(), jets[1].eta(), endcap, forward)
            && (_h_dijet_mass_endcap_forward_fine->xMin() <= dijet_mass)
            && (_h_dijet_mass_endcap_forward_fine->xMax() > dijet_mass)
            ) {
            _h_x_endcap_forward_x1->fill(x1, weight * x1);
            _h_x_endcap_forward_x2->fill(x2, weight * x2);
          }
          if (
            match_regions(jets[0].eta(), jets[1].eta(), forward, forward)
            // forward-forward histogram had a low limit set too low on purpose
            // FIXME once a new production is done
            && (19 /* GeV */ <= dijet_mass)
            && (_h_dijet_mass_forward_forward_fine->xMax() > dijet_mass)
            ) {
            _h_x_forward_forward_x1->fill(x1, weight * x1);
            _h_x_forward_forward_x2->fill(x2, weight * x2);
          }
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {
      scale(_h_dijet_mass_nbarrel_forward, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_pbarrel_forward, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_endcap_forward, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_forward_forward, crossSection()/sumOfWeights());

      scale(_h_dijet_mass_nbarrel_forward_fine, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_pbarrel_forward_fine, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_endcap_forward_fine, crossSection()/sumOfWeights());
      scale(_h_dijet_mass_forward_forward_fine, crossSection()/sumOfWeights());

      scale(_h_x_nbarrel_forward_x1, crossSection()/sumOfWeights());
      scale(_h_x_nbarrel_forward_x2, crossSection()/sumOfWeights());
      scale(_h_x_pbarrel_forward_x1, crossSection()/sumOfWeights());
      scale(_h_x_pbarrel_forward_x2, crossSection()/sumOfWeights());
      scale(_h_x_endcap_forward_x1, crossSection()/sumOfWeights());
      scale(_h_x_endcap_forward_x2, crossSection()/sumOfWeights());
      scale(_h_x_forward_forward_x1, crossSection()/sumOfWeights());
      scale(_h_x_forward_forward_x2, crossSection()/sumOfWeights());
    }

    //@}


  private:

    // Data members like post-cuts event weight counters go here


    /// @name Histograms
    //@{
    Histo1DPtr _h_dijet_mass_nbarrel_forward;
    Histo1DPtr _h_dijet_mass_pbarrel_forward;
    Histo1DPtr _h_dijet_mass_endcap_forward;
    Histo1DPtr _h_dijet_mass_forward_forward;

    Histo1DPtr _h_dijet_mass_nbarrel_forward_fine;
    Histo1DPtr _h_dijet_mass_pbarrel_forward_fine;
    Histo1DPtr _h_dijet_mass_endcap_forward_fine;
    Histo1DPtr _h_dijet_mass_forward_forward_fine;

    Histo1DPtr _h_x_nbarrel_forward_x1;
    Histo1DPtr _h_x_nbarrel_forward_x2;
    Histo1DPtr _h_x_pbarrel_forward_x1;
    Histo1DPtr _h_x_pbarrel_forward_x2;
    Histo1DPtr _h_x_endcap_forward_x1;
    Histo1DPtr _h_x_endcap_forward_x2;
    Histo1DPtr _h_x_forward_forward_x1;
    Histo1DPtr _h_x_forward_forward_x2;
    //@}


  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(STAR_FORWARD);


}
